package cController

import (
	"gitee.com/csingo/cComponents"
)

type ControllerComponent struct{}

func (i *ControllerComponent) Inject(instance any) bool {
	if container.Is(instance) {
		return container.Save(instance)
	}

	return false
}

func (i *ControllerComponent) InjectConf(config cComponents.ConfigInterface) bool {
	return false
}

func (i *ControllerComponent) Load() {}

func (i *ControllerComponent) Listen() []*cComponents.ConfigListener {
	return []*cComponents.ConfigListener{}
}

var Component = &ControllerComponent{}
