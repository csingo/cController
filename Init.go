package cController

import (
	"fmt"
	"gitee.com/csingo/cLog"
	"github.com/gin-gonic/gin"
	"net/http"
	"reflect"
)

func GetGinHandlerFunc(app, name, method string) (gin.HandlerFunc, error) {
	var result gin.HandlerFunc
	var err error

	index := app + "." + name
	instance := container.Get(index)

	if instance == nil {
		cLog.WithContext(nil, map[string]any{
			"source": "cController.GetGinHandlerFunc",
			"app":    app,
			"name":   name,
			"method": method,
		}).Error("获取 controller 异常")
		err = fmt.Errorf("获取 controller 异常")
		return result, err
	}

	instanceMethod := reflect.ValueOf(instance).MethodByName(method)
	if !instanceMethod.IsValid() || instanceMethod.IsNil() {
		cLog.WithContext(nil, map[string]any{
			"source": "cController.GetGinHandlerFunc",
			"app":    app,
			"name":   name,
			"method": method,
		}).Error("获取 controller method 异常")
		err = fmt.Errorf("获取 controller method 异常")
		return result, err
	}

	result = func(ctx *gin.Context) {
		params := []reflect.Value{
			reflect.ValueOf(ctx),
		}

		instanceResult := instanceMethod.Call(params)
		if len(instanceResult) != 1 {
			cLog.WithContext(ctx, map[string]any{
				"source": "cController.GetGinHandlerFunc",
				"app":    app,
				"name":   name,
				"method": method,
			}).Error("controller call result 异常")
			if !ctx.IsAborted() {
				ctx.AbortWithStatus(http.StatusInternalServerError)
			}
		}

		if !instanceResult[0].IsNil() {
			cLog.WithContext(ctx, map[string]any{
				"source": "cController.GetGinHandlerFunc",
				"app":    app,
				"name":   name,
				"method": method,
				"result": instanceResult[0].Interface().(error).Error(),
			}).Error("controller call result 异常")
			if !ctx.IsAborted() {
				ctx.AbortWithStatus(http.StatusInternalServerError)
			}
		}
	}

	return result, err
}
