package cController

type ControllerInterface interface {
	ControllerName() (app, name string)
}
